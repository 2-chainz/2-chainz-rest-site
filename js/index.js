getQuote();

function getQuote() {
  fetch("https://api.chainz.rest/quote", { method: "GET" })
    .then(resp => resp.json())
    .then(function(data) {
      document.getElementById("quote").innerHTML = data.quote;

      const tweet = encodeURIComponent(
        `"${data.quote}" -@2chainz via https://2.chainz.rest`
      );
      document.getElementById(
        "tweet"
      ).href = `https://twitter.com/intent/tweet?text=${tweet}`;
    });
}
